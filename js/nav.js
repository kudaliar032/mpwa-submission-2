import './component/my-nav.js';
import {homeScript} from './pages/home.js';
import {teamsScript} from "./pages/teams.js";
import {favoriteTeams} from "./pages/favorites.js";

export const loadNav = () => {
  document.querySelectorAll(".topnav, .sidenav").forEach(elm => {
    const navMenu = document.createElement("my-nav");
    elm.appendChild(navMenu);
  });

  // Click menu
  document.querySelectorAll(".sidenav a, .topnav a").forEach(elm => {
    elm.addEventListener("click", event => {
      // Tutup sidebar
      const sidenav = $(".sidenav");
      M.Sidenav.getInstance(sidenav).close();

      // Memuat halaman yang diklik
      const page = event.target.getAttribute("href").substr(1);
      loadPage(page);
    })
  })
}

export const loadPage = page => {
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState === 4) {
      const content = $("#body-content");
      if (this.status === 200) {
        content.html(xhttp.responseText);
        switch (page) {
          case 'home':
            homeScript();
            break;
          case 'teams':
            teamsScript();
            break;
          case 'favorites':
            favoriteTeams();
            break;
          default:
            homeScript();
        }
      } else if (this.status === 404) {
        content.html(`<h1>Halaman tidak ditemukan</h1>`)
      } else {
        content.html(`<h1>Halaman tidak dapat diakses</h1>`);
      }
    }
  };
  xhttp.open("GET", "pages/" + page + ".html", true);
  xhttp.send();
}
