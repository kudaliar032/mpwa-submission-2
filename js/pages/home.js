export async function homeScript() {
  const baseUrl = "https://api.football-data.org/v2";
  $('.tabs').tabs();

  if ("caches" in window) {
    try {
      const [standingsRes, scorersRes] = await Promise.all([
        caches.match(`${baseUrl}/competitions/2014/standings`),
        caches.match(`${baseUrl}/competitions/2014/scorers`)
      ]);
      if (standingsRes && scorersRes) {
        const [standings, scorers] = await Promise.all([
          standingsRes.json(),
          scorersRes.json()
        ]);
        renderElement(standings, scorers);
      }
    } catch (e) {
      console.error(e);
    }
  }

  try {
    const [{data: standings}, {data: scorers}] = await Promise.all([
      axios(`${baseUrl}/competitions/2014/standings`, {
        headers: {
          'X-Auth-Token': '8af716388f9545d6812295235815f147'
        }
      }),
      axios(`${baseUrl}/competitions/2014/scorers`, {
        headers: {
          'X-Auth-Token': '8af716388f9545d6812295235815f147'
        }
      })
    ]);
    renderElement(standings, scorers);
  } catch (e) {
    console.error(e);
  }
}

function renderElement(standings, scorers) {
  // Standings
  const standingElement = $(`#standings tbody`);
  const standingTable = standings.standings[0].table.map(val => {
    return `<tr>
        <td class="valign-wrapper">
          <img src="${val.team.crestUrl.replace(/^http:\/\//i, 'https://')}" alt="" class="responsive-img club"/>
          ${val.team.name}
        </td>
        <td>${val.playedGames}</td>
        <td>${val.won}</td>
        <td>${val.draw}</td>
        <td>${val.lost}</td>
        <td>${val.goalsFor}</td>
        <td>${val.goalsAgainst}</td>
        <td>${val.points}</td>
      </tr>`
  })
  standingElement.html(standingTable);

  // Top Scorers
  const scorersElement = $(`#scorers tbody`);
  const scorersTable = scorers.scorers.map(val => {
    return `<tr>
        <td class="center-align">${val.player.name}</td>
        <td class="center-align">${val.team.name}</td>
        <td class="center-align">${val.numberOfGoals}</td>
      </tr>`
  })
  scorersElement.html(scorersTable);
}