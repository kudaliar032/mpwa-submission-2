$(document).ready(() => {
  detailFunction();
})

function detailFunction() {
  return new Promise(async (resolve, reject) => {
    $('.tabs').tabs();

    const baseUrl = "https://api.football-data.org/v2";
    const urlParams = new URLSearchParams(window.location.search);
    const idParam = urlParams.get("id");
    const inDb = await getFavoriteTeam(idParam);

    try {
      if (inDb) {
        renderDetail(inDb);
      } else {
        if ("caches" in window) {
          try {
            const detailRes = await caches.match(`${baseUrl}/teams/${idParam}`);
            if (detailRes) {
              const detail = await detailRes.json();
              renderDetail(detail);
              resolve(detail);
            }
          } catch (e) {
            console.error(e);
            reject(e);
          }
        }

        try {
          const {data: detail} = await axios(`${baseUrl}/teams/${idParam}`, {
            headers: {
              'X-Auth-Token': '8af716388f9545d6812295235815f147'
            }
          });
          renderDetail(detail);
          resolve(detail);
        } catch (e) {
          console.log(e);
          reject(e);
        }
      }
    } catch (e) {
      console.log("Database Error!");
    }
  })
}

function renderDetail(detail) {
  // Logo
  $('#team-logo').attr({
    alt: `${detail.name}`,
    src: `${detail.crestUrl.replace(/^http:\/\//i, 'https://')}`
  })

  // About
  const aboutList = {
    name: "Name",
    shortName: "Short name",
    tla: "Three letter",
    phone: "Phone",
    website: "Website",
    email: "Email address",
    founded: "Founded",
    clubColors: "Club colors",
    venue: "Venue",
    address: "Address"
  }
  const aboutElement = $('#team-about');
  const aboutTable = ['name', 'shortName', 'tla', 'phone', 'website', 'email', 'founded', 'clubColors', 'venue', 'address'].map(val => {
    return `
        <tr>
          <td><b>${aboutList[val]}</b></td>
          <td>${detail[val]}</td>
        </tr>
        `
  })
  aboutElement.html(aboutTable);

  // Active competition
  const competitionElement = $('#team-competition tbody');
  const competitionTable = detail.activeCompetitions.map(val => {
    return `
        <tr>
          <td>${val.name} (${val.code})</td>
          <td>${val.area.name || '-'}</td>
        </tr>
        `
  })
  competitionElement.html(competitionTable);

  // Team squad
  const squadElement = $('#team-squad tbody');
  const squadTable = detail.squad.map(val => {
    return `
        <tr>
          <td>${val.name}</td>
          <td>${val.shirtNumber || '-'}</td>
          <td>${val.position || 'Other'}</td>
          <td>${val.nationality}</td>
        </tr>
        `
  })
  squadElement.html(squadTable);
}