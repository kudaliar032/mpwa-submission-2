export async function favoriteTeams() {
  try {
    const favorites = await getFavorites();
    if (favorites.length > 0) {
      renderElement(favorites);
    } else {
      $('#favorites').html(`<h4 class="center grey-text darken-4">You don't have favorite teams!</h4>`)
    }
  } catch (e) {
    console.log(e);
  }
}

function renderElement(favorites) {
  const favoritesElement = $('#favorites');
  const teamCard = favorites.map(val => {
    return `
          <div class="card horizontal">
            <div class="card-image">
              <img src="${val.crestUrl.replace(/^http:\/\//i, 'https://')}" class="responsive-img" style="width: 10rem; padding: 25px" alt="${val.name}">
            </div>
            <div class="card-stacked">
              <div class="card-content">
                <p><b>Name</b> ${val.name} (${val.tla})</p>
                <p><b>Vanue</b> ${val.venue}</p>
                <p><b>Founded</b> ${val.founded}</p>
                <p><b>Colors</b> ${val.clubColors}</p>
                <p><b>Address</b> ${val.address}</p>
              </div>
              <div class="card-action right-align">
                <a href="detail.html?id=${val.id}" class="waves-effect waves-light btn grey darken-3">Detail</a>
              </div>
            </div>
          </div>
        `
  })
  favoritesElement.html(teamCard);
}