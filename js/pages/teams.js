export async function teamsScript() {
  const baseUrl = "https://api.football-data.org/v2";

  if ("caches" in window) {
    try {
      const teamsRes = await caches.match(`${baseUrl}/competitions/2014/teams`);
      if (teamsRes) {
        const teams = await teamsRes.json();
        renderElement(teams);
      }
    } catch (e) {
      console.error(e);
    }
  }

  try {
    const {data: teams} = await axios(`${baseUrl}/competitions/2014/teams`, {
      headers: {
        'X-Auth-Token': '8af716388f9545d6812295235815f147'
      }
    });
    renderElement(teams);
  } catch (e) {
    console.error(e);
  }
}

function renderElement(teams) {
  const teamsElement = $('#teams');
  const teamsCard = teams.teams.map(val => {
    return `
          <div class="card horizontal">
            <div class="card-image">
              <img src="${val.crestUrl.replace(/^http:\/\//i, 'https://')}" class="responsive-img" style="width: 10rem; padding: 25px" alt="${val.name}">
            </div>
            <div class="card-stacked">
              <div class="card-content">
                <p><b>Name</b> ${val.name} (${val.tla})</p>
                <p><b>Vanue</b> ${val.venue}</p>
                <p><b>Founded</b> ${val.founded}</p>
                <p><b>Colors</b> ${val.clubColors}</p>
                <p><b>Address</b> ${val.address}</p>
              </div>
              <div class="card-action right-align">
                <a href="detail.html?id=${val.id}" class="waves-effect waves-light btn grey darken-3">Detail</a>
              </div>
            </div>
          </div>
        `
  })
  teamsElement.html(teamsCard);
}