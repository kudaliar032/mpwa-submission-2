class MyNav extends HTMLElement {
  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = `
      <li><a class="waves-effect" href="#home">Home</a></li>
      <li><a class="waves-effect" href="#teams">Teams</a></li>
      <li><a class="waves-effect" href="#favorites">Favorites</a></li>
    `;
  }
}

customElements.define('my-nav', MyNav);