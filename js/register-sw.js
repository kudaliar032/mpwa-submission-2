export function registerServiceWorker() {
  return navigator.serviceWorker.register("service-worker.js").then(() => {
    console.log("ServiceWorker Success!");

    if ("Notification" in window) {
      Notification.requestPermission().then(result => {
        if (result === "denied") {
          console.error("Notification Denied!");
          return;
        } else if (result === "default") {
          console.error("Notification Access Closed!");
          return;
        }
        console.log("Notification Allowed!");

        if (("PushManager" in window)) {
          navigator.serviceWorker.getRegistration().then(registration => registration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array("BHVJYG2WuUA7BxDZC27ZZpRHml_fhoOwufd8LgJy1KdJ0h2YyhQht6kCdyxGNGRpabobboSLXQ__2MyRU8yhtqo")
          })).then(subscribe => {
            console.log('Endpoint: ', subscribe.endpoint);
            console.log('p256dh: ', btoa(String.fromCharCode.apply(null, new Uint8Array(subscribe.getKey('p256dh')))));
            console.log('auth: ', btoa(String.fromCharCode.apply(null, new Uint8Array(subscribe.getKey('auth')))));
          }).catch(error => {
            console.error("Subscribe Failed!", error.message);
          })
        }
      })
    } else {
      console.error("Notification Not Support!");
    }
  }).catch(err => {
    console.error("ServiceWorker Failed!", err);
  })
}

function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/-/g, '+')
    .replace(/_/g, '/');
  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);
  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}