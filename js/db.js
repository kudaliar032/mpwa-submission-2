const dbPromised = idb.open("my-footbal", 1, upgradeDb => {
  const favoriteObjectStore = upgradeDb.createObjectStore("favorite", {
    keyPath: "id"
  });
  favoriteObjectStore.createIndex("id", "id", {unique: true});
})

function pickFavorite(team) {
  dbPromised.then(db => {
    const tx = db.transaction("favorite", "readwrite")
    const store = tx.objectStore("favorite");
    store.add(team);
    return tx.complete;
  }).then(() => {
    console.log("Favorite selected!");
  });
}

function getFavoriteTeam(id) {
  return new Promise((resolve, reject) => {
    dbPromised.then(db => {
      const tx = db.transaction("favorite", "readonly")
      const store = tx.objectStore("favorite")
      return store.get(parseInt(id))
    }).then(team => {
      resolve(team);
    }).catch(error => {
      reject(error);
    })
  })
}

function getFavorites() {
  return new Promise((resolve, reject) => {
    dbPromised.then(db => {
      const tx = db.transaction("favorite", "readonly");
      const store = tx.objectStore("favorite");
      return store.getAll();
    }).then(favorite => {
      resolve(favorite);
    }).catch(error => {
      reject(error);
    })
  })
}

function removeFavorite(id) {
  dbPromised.then(db => {
    const tx = db.transaction("favorite", "readwrite");
    const store = tx.objectStore("favorite");
    store.delete(id);
    return tx.complete;
  }).then(() => {
    console.log("Favorite deleted!");
  })
}