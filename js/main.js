import {loadNav, loadPage} from "./nav.js";

$(document).ready(() => {
  // Active sidebar
  const sidebarElems = $('.sidenav');
  M.Sidenav.init(sidebarElems);
  loadNav();

  // Load default page
  let page = window.location.hash.substr(1);
  if (page === "") page = "home";
  loadPage(page);
})