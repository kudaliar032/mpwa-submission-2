const webPush = require("web-push");
const vapidKeys = require("./vapid-key.json");

webPush.setVapidDetails(
  'mailto:adit@mail.com',
  vapidKeys.publicKey,
  vapidKeys.privateKey
)

const pushSubscription = {
  "endpoint": "https://fcm.googleapis.com/fcm/send/dAFSuWiIKFw:APA91bHDmYIvV8N32ykm_eqW42lm-GKcOPj7IoTdR0aSnzLaw-x0arkfrfW-qdyo0b15p4yeBn9twJTNMXmfpmVSpYoHq3vuPmP7SI8lyTK76jI3bAFjGCjfLwYyfEDaeEu0mKK3KszE",
  "keys": {
    "p256dh": "BHGiK5Ew4T4xzHckJJ+p3tDldF9Q+jbU3taOmXMrDSwTGDe3dSiDkB+Y1fEkX29JReFNi46ZSUkU+uDyzyysqTo=",
    "auth": "MrFsAzf5moC07pgCfEhMgA=="
  }
}

const payload = "Match Recap: FCB (5) vs RMA (0)";

const options = {
  gcmAPIKey: "10407514647",
  TTL: 60
}

webPush.sendNotification(pushSubscription, payload, options);