const CACHE_NAME = "my-football-v1";
const urlsToCache = [
  "/",
  "/detail.html",
  "/index.html",
  "/manifest.json",
  "/css/custom.css",
  "/css/materialize.min.css",
  "/img/icon.png",
  "/img/icons/icon-72x72.png",
  "/img/icons/icon-96x96.png",
  "/img/icons/icon-128x128.png",
  "/img/icons/icon-144x144.png",
  "/img/icons/icon-152x152.png",
  "/img/icons/icon-192x192.png",
  "/img/icons/icon-384x384.png",
  "/img/icons/icon-512x512.png",
  "/js/db.js",
  "/js/idb.js",
  "/js/jquery.min.js",
  "/js/main.js",
  "/js/materialize.min.js",
  "/js/nav.js",
  "/js/register-sw.js",
  "/js/component/my-nav.js",
  "/js/pages/detail.js",
  "/js/pages/favorites.js",
  "/js/pages/home.js",
  "/js/pages/teams.js",
  "/pages/favorites.html",
  "/pages/home.html",
  "/pages/teams.html",
  "https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js",
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  "https://fonts.gstatic.com/s/materialicons/v50/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2"
];

self.addEventListener("install", event => {
  event.waitUntil(
    caches.open(CACHE_NAME).then(cache => {
      return cache.addAll(urlsToCache);
    })
  )
})

self.addEventListener("fetch", event => {
  const baseUrl = "https://api.football-data.org/v2/";

  if (event.request.url.indexOf(baseUrl) > -1) {
    event.respondWith(
      caches.open(CACHE_NAME).then(cache => fetch(event.request)
        .then(response => {
          cache.put(event.request.url, response.clone());
          return response;
        })
      )
    )
  } else {
    event.respondWith(
      caches.match(event.request, {ignoreSearch: true}).then(response => response || fetch(event.request))
    )
  }
})

self.addEventListener("activate", event => {
  event.waitUntil(
    caches.keys().then(cacheNames => Promise.all(
      cacheNames.map(cacheName => {
        if (cacheName !== CACHE_NAME) {
          console.log(`ServiceWorker: cache ${cacheName} dihapus`);
          return caches.delete(cacheName);
        }
      })
    ))
  )
})

self.addEventListener("push", event => {
  let body;
  if (event.data) {
    body = event.data.text();
  } else {
    body = "No payload!";
  }

  const options = {
    body,
    icon: 'img/icon.png',
    vibrate: [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      primaryKey: 1
    }
  }

  event.waitUntil(
    self.registration.showNotification("LaLiga Update", options)
  )
})